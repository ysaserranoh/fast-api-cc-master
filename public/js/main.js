
//Obtengo la ruta dela API
const url = 'http://localhost:8081/api/members'

//Objeto para llevar el status de los input del formulario, para elaborar la condición para cambiar el estado del botón "Save"
let validation = {
  firstName: false,
  lastName: false,
  address: false,
  ssn: false
}

/*Función GET * para obtener  los  members
Utilicé un método global llamado fetch, es una forma fácil y rápida de obtener recursos de forma asíncrona por la red. Está me permitió establecer la conexión, 
obtener y enviar datos a la API, para evitar el uso repetitivo de las promesas de la forma .then, usé Async away.*/
async function getMembers() {
  try {
    let response = await fetch(url)
    let members = await response.json()
    let html = document.querySelector('#items')
    html.innerHTML = ''
    for (let member of members) {
      html.innerHTML += `
            <td>${ member.firstName}</td>
            <td>${ member.lastName}</td>
            <td>${ member.address}</td>
            <td>${ member.ssn}</td>
            `
    }
  } catch (error) {
    console.log('There was a problem with the request' + error.message)
  }
}

//Función Post para registrar Members
async function addMember() {
  if (validateForm()) {
    try {
      let form = document.getElementById('form') //Detectamos el formulario
      let messageHtml = document.getElementById('message')
      let members = new FormData(form) //Los objetos FormData representan los datos del formulario HTML

      //Fetch recibe la url de la API, haciendo promesas traemos  los datos de la API
      let data = await fetch(url, {
        method: 'POST',
        body: members, //Datos del formulario
      })

      if (data.ok) {
        message.innerHTML = `
          <div style="color: green">
          <div style="color: white; text-align:center; padding: 10px; background-color: #81C784 !important; height: 20px; font-size:13px">
          Success
          </div>
          `
        getMembers()
      } else {
        let { message } = await data.json()
        messageHtml.innerHTML = `
          <div style="color: white; text-align:center; padding: 10px; background-color: #EF9A9A !important; height: 20px; font-size:13px">
          ${message}
          </div>
           `
      }
    } catch (error) {
      let messageHtml = document.getElementById('message')
      messageHtml.innerHTML = '<div style="color: red">Error</div>'
    }
  } else {
    let messageHtml = document.getElementById('message')
    messageHtml.innerHTML = '<div style="color: red">Error de validacion</div>'
  }
}

//Llamada a la función getMembers *traer members* cuando el evento load  de window se dispara
window.onload = () => {
  getMembers()
}

//Resetear input del formulario
function clearForm() {
  document.getElementById("form").reset()
}
//Manejar estado del botón "Save"
function checkForm() {
  var btn = document.getElementById('btn')
  if (!validation.firstName || !validation.lastName || !validation.address || !validation.ssn) {
    document.getElementById("form").btn.disabled = true
    document.getElementById("form").btn.value = "Disabled"
    return true
  } else {
    document.getElementById("form").btn.disabled = false
    document.getElementById("form").btn.value = "Save"
  }
}

//Validaciones del formulario realizadas con expresiones regulares
function validarFirstName() {
  var first = document.getElementById('firstName').value
  if (validarString(first)) {
    document.getElementById('name').innerHTML = "<i style='color: green; padding: 10px' class='fas fa-check-circle'/>"
    validation.firstName = true; //Devuele verdadero si la validación del input es correcta, me permite hacer la condición para ejecutar la funcionalidad "DISABLED" del botón "save",  
    checkForm() //Permite cambiar el estado del botón "Save"
    return true;
  } else {
    document.getElementById('name').innerHTML = "<i style='color: red; padding: 10px' class='fas fa-times-circle'/>"
    validation.firstName = false
    checkForm()
    return false
  }
}

function validarLastName() {
  var last = document.getElementById('lastName').value
  if (validarString(last)) {
    document.getElementById('last').innerHTML = "<i style='color: green; padding: 10px' class='fas fa-check-circle'/>"
    validation.lastName = true
    checkForm()
    return true
  } else {
    document.getElementById('last').innerHTML = "<i style='color: red; padding: 10px' class='fas fa-times-circle'/>"
    validation.lastName = false
    checkForm()
    return false
  }
}

function validarAddress() {
  var address = document.getElementById('address').value
  if (validarAddrss(address)) {
    document.getElementById('addr').innerHTML = "<i style='color: green; padding: 10px' class='fas fa-check-circle'/>"
    validation.address = true
    checkForm()
    return true
  } else {
    document.getElementById('addr').innerHTML = "<i style='color: red; padding: 10px' class='fas fa-times-circle'/>"
    validation.address = false
    checkForm()
    return false
  }
}

function validarSsn() {
  var ssn = document.getElementById('ssn').value
  if (validarMask(ssn)) {
    document.getElementById('codssn').innerHTML = "<i style='color: green; padding: 10px' class='fas fa-check-circle'/>"
    validation.ssn = true
    checkForm()
    return true
  } else {
    document.getElementById('codssn').innerHTML = "<i style='color: red; padding: 10px' class='fas fa-times-circle'/>"
    validation.ssn = false
    checkForm()
    return false
  }
}

//Expresiones regulares - Validar lastname, firstname
function validarString(valor) {
  return valor != null && valor.length > 1 && /^([a-zA-Z]+)$/.test(valor)
}

//Expresiones regulares - Validar address
function validarAddrss(valor) {
  return valor != null && valor.length > 1 && /^[a-zA-Z0-9\s\,\''\-]*$/.test(valor)
}

//Expresiones regulares - Validar SSN
function validarMask(valor) {
  return valor != null && valor.length > 1 && /^([0-9]{3})-([0-9]{2})-([0-9]{4})$/.test(valor)
}

//Validar el formulario antes de presionar "save"
function validateForm() {
  return validarFirstName() && validarLastName() && validarAddress() && validarSsn()
}

//Obtener  modal
var modal = document.getElementById("myModal")

//Obtenener el botón que abre el modal
var btn = document.getElementById("myBtn")

//Obtener el elemento <span> que cierra el modal
var span = document.getElementsByClassName("close")[0]

//Al dar click al botón abre la modal
btn.onclick = function () {
  modal.style.display = "block"
}

//Cerrar modal, limpiar formulario
span.onclick = function () {
  clearForm();
  document.getElementById('name').innerHTML = " "
  document.getElementById('last').innerHTML = " "
  document.getElementById('addr').innerHTML = " "
  document.getElementById('codssn').innerHTML = " "
  document.getElementById('message').innerHTML = " "
  modal.style.display = "none"
}

// Cerrar modal al hacer click fuera de la ventana
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none"
  }
}

