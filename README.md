# Entrevista laboral

##test terminado

*Home
![Home](/uploads/e8202a2aa7ca13382b38aa2ecfda0217/Home.png)

*Formulario
![form](/uploads/320faec03b609ef33b5bdb62c4162dc9/form.png)

*Register of member
![register](/uploads/25b5f594d5121cdfcf7be9f67d9fb83d/register.png)

*List of members
![tabla](/uploads/f3040a0e02fabc7ec1f53eb1c86f7a0f/tabla.png)

Esta prueba fue desarrollada bajo las tecnologías:

*  **Javascript**
*  **CSS3**
*  **HTML5**
*  **RESTful Services / APIS**
*  **JSON**

Nota
*Las vistas no son completamente mobile responsive


## Requisitos
* Crear en una nueva carpeta el siguiente sitio web
![alt text](https://gitlab.com/arsenyel/fast-api-cc/raw/master/design.png "Diseño web")
* La seccion del formulario debe ser maquetado y controlado **exclusivamente con JS (DOM)**
* Los datos del formulario deben ser enviados a la API, la tabla de la derecha debe recibir los datos de la misma al cargarse el sitio y luego de cada insercion exitosa
* El boton reset debe limpiar los campos del formulario
* El boton save debe enviar los datos a la API

## API
La pagina debe poder comunicarse con la API de este repositorio. La misma consta de dos endpoints

* GET http://localhost:8081/api/members - para obtener los miembros
* POST http://localhost:8081/api/members - para añadir un nuevo miembro

### Start API server
* Clonar este repositorio
* Instalar las dependencias
* Usar el comando "serve"

### Validaciones de la API
* **firstName, lastName y address:** Mas de 1 caracter, sin espacios a los costados (trim)
* **ssn:** Tener el formato ###-##-#### (cada # es un numero, los guiones son obligatorios)
* Si el front no cumple las validaciones debe deshabilitar el boton de enviar

## Condiciones y tips
* Los colores y formas son solo a caracter ilustrativo
* No es necesario que sea mobile responsive
* **NO USAR** ningun framework, libreria o dependencia. Solo debe usarse Vanilla (JS puro) o CSS
* No es necesaria compatibilidad con IE o Edge
* Puede usar ES6 sin problemas
* Puede usar HTML5 sin ningun problema
* Se puede usar google :D
* Se puede usar POSTMAN para ver el contenido del archivo Fast API CC.postman_collection.json comodamente, igualmente su contenido es legible y sirve de referencia para la comunicacion con la API
